const express = require("express");

const router = express.Router();

router.get("/courses", (request, response)=> {
    response.status(200).json({
        message: "GET All course"
    })
});

router.get('/courses/:courseId', (request, response)=> {
    let courseId = request.params.courseId;
    response.status(200).json({

        message: "GET course Id = " + courseId
    })
});

router.post("/courses", (request, response)=> {
    response.status(200).json({
        message: "Create course"
    })
});

router.put('/courses/:courseId', (request, response)=> {
    let courseId = request.params.courseId;
    response.status(200).json({

        message: "Update course Id = " + courseId
    })
});

router.delete('/courses/:courseId', (request, response)=> {
    let courseId = request.params.courseId;
    response.status(200).json({

        message: "delete course Id = " + courseId
    })
});

module.exports = router;